let userName = prompt('Enter your name');
while (userName.trim() === '') {
    userName = prompt('Please enter correct name', `${userName}`)
}

let userAge = prompt('Enter your age');
while (userAge.trim() === '' || isNaN(userAge)) {
    userAge = prompt('Please enter correct age', `${userAge}`)
}

if (userAge < 18) {
    alert('You are not allowed to visit this website');
}

else if ((userAge >= 18) && (userAge <= 22)) {
    let userAccept = confirm('Are you sure you want to continue?');
    if (userAccept) {
        alert(`Welcome, ${userName}`);
    } else {
        alert('You are not allowed to visit this website');
    }
}
else {
    alert(`Welcome, ${userName}`);
}
